Location: West of the Sword Coast
What: A city


## Places of Interest

- [The Rainbow Cloaks](The%20Rainbow%20Cloaks.md)
- [[The Slums]]


SE - Chasm District
	- Slums, chaotic area
NE - CloakTower
	- Nobles private houses
NW - Blue Lake
	- Was Black, heavily polluted
	- oozes and zombies kept coming out
	- been cleaned up 
	- Used to be middle-classes but rich merchants are moving in
SW - Protectors Enclave
	- Don't mess with these folks

About 15 years prior, a Red Dragon made [[Mount Hotenow]] his lair, causing a large eruption and fled the area and attacked NeverWinter which created the chasm and destroyed much of the SE district, and the protective walls. This allowed orcs to attack and cause trouble.



Tags:
[[Place]]


 