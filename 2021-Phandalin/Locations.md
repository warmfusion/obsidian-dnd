## [[Neverwinter]]

The shops you have visited:  

-   The Crimson lich, pub for city guards
-   The Manycoins Bank
-   Goldcap Smithy
-   Main's Book Shop
-   Gimble's Awesome Shop of Tricks, Jokes and Novelties
-   Seven Suns Coster Market
-   The Nook

Places you have heard of:  

-   Wincanton's Emporium
-   The Sage's Workshop
-   The Cloaktower
-   Thunderhammer & Stormshield Armories
-   Stronginthearm And Burley
-   The Hall of Justice
-   The Craft Hall
-   The Hard Rock Cafe


## The Lurkwood

- The vault of Quentins Order [Game 30 - 09-06-2022](Game%2030%20-%2009-06-2022.md)
	- Ambush was Just north of Xantharls Keep, beside The Lurkwood