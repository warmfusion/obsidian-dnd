

# Quest list

- Mysterious Item from the Jaded Lion Ship (Lighthouse, game26-29), being saught after by;
	- [ ] Dragonborn
	- [ ] Dwarves from [Steel Knuckle Clan](Steel%20Knuckle%20Clan)

- [ ] Shadybeards are doing something suspicious
	- Seem to be collecting artifacts(?)

## Side Quests?

Blue-Cloaked Man [Mikkra Bodello](Mikkra%20Bodello.md) - [Moonstone inn](Moonstone%20inn.md), Neverwinter
- [ ] Blue Lake District [Game 29]
	- [x] [Moonstone Mask](Moonstone%20Mask.md) - Red box 25x25x10cm covered in velvet holding a, mask is the priority
	- [x] Warerat issue in blue lake district
- [ ] Steel Knuckles Clan House; Dwarves being supicious
- [x] Travel to Mage guild to obtain 1000gp after clearing the illithid portal in the mines
	 - Take 'Strange Coin' given to us, held by Olran, as proof

[Strong-in-the-arm and burley](Strong-in-the-arm%20and%20burley.md), Neverwinter
- [ ] Fire Giants Steel - 80gp per ingot
	- Imbued with Fire attributes/enchangements
	- Fire giants in the north	
- [ ] Bulette Exoskeleton - 90gp
	- Would make nice armor
	- Land sharks; pretty rare
- [ ] Winter wolf pelt - 25gp
	- Magical creatures; associated with the cold
	- From the north, in mountains

[salazar the wizard](salazar%20the%20wizard.md)  - Cloak Tower, Neverwinter
- [ ] Laboratory - retrieve a Grimoire from the dungeon of a wizard
	- Rosentha / Amphail - first chain of mountains north of water deep
	- Wizard killed by trap
-  [ ] Fire drakes in mount hotenow Volcano
	- bring back a ~baby fire drake~ scale
	- they can be bound to an individual 
	- Muradin has a good idea - get one for the group

[[Belfasastar]]? Blue dragon in charge of Quentins group
- [ ] Some dangerous artifacts being taken ot vault were stolen
	- Very far north, longsaddle/zanthars keep
-


## Completed

Neverwinter

- Meet sansar the wizard at the cloaktower for some employment oportunities
- 


Wave Echo Cave
- There is an underground sea which may be the entrance to the Under Dark
	- A devilish/hellscape of terrors etc