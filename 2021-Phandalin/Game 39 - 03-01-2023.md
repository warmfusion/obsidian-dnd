Previous: [Game 38 - 06-12-22](Game%2038%20-%2006-12-22.md)
Next: [Game 40 - 17-01-2023](Game%2040%20-%2017-01-2023.md)
Goal: Find out about the [Moonstone Mask](Moonstone%20Mask.md)

---

We drag one of the hostages we've taken into the main space.

After looking over the character, I notice a symbol on a ring on his hand a [[Zhentarim]], which he remembers to be a mecenary secret group.

Thomas takes one of the rings from one of the fallen folks and puts it in his bag for later.

Olran and Muradin take a break for a bit.

Quentin notices the sound of Chanting behind a locked/closed door.


Thomas attempts to check the pockets of the halfling, who takes the oportunity to cast some sort of posion attack. Thomas manages to miss much of the pain, but alas gets affected by the poision.

Thomas decides to quaff the v2.0 Gimbal Posion, he heals, but for some reason, he is now Purple.

Thomas inspires everyone, and we head down a long corridor, and up some stairs.

We spot a glowing gliph on a wall Falcia fails to interpret what it is, thomas however recognises it from when we visited [salazar the wizard](salazar%20the%20wizard.md) - it was a rune in an elevator in some corridors.

Parts of the runes are moving clockwise and parts moving counter clockwise.

Olran, Muradin and Quentin prod the mechanism, and get transported to another part of the sewer system, and in a room beyond see a fire elemental being counjoured...

Thomas, who isn't keen on taking the trip, gets a telepathic message from Muradin asking for help.

He touches the rune, and prepares an eldritch blast...

***Roll For Initiative***

Quentin gets caught on fire.
Olran uses  Bead of Force which traps the Fire Elemental in a ball.

He rushes out and gets hammered by a bunch of folks.

Thomas protects himself with a spell and a bit of a heal before he notices Olran badly hurt outside.

Muradrin has a go at the dwarfs outside and deals some beefy damage, but not enough to kill them.

A few rounds of back and forth, and Thomas gets the idea to kick one of the halflings into the center of the room, where a trap circle and an ominous 'crack of fire' is being created by some nearby gnomes. 

The halfing falls into the pit and is turned to ash.

Muradin, Falcia and Quentin get the idea, and subsequently push a Gnome, halfling and two dwarves into the pit

We move into the next room, and see a mask on an altar; We recognise it as the mask of the dead - [[Myrkul The God Of Death]]

![https://cdn.discordapp.com/attachments/1040643087552221297/1059945211029102642/myrkull-mask.webp](https://cdn.discordapp.com/attachments/1040643087552221297/1059945211029102642/myrkull-mask.webp)

The Mask disapeared in a flash/disk of light, a hand reached from the disk and returned into its portal with the mask.

Olran attempts to grab wildly into the portal that remains open, too small for him to dive through, and manages to grab something; Pulling hard, but alas wasn't able to pull it through.

Muradin, uses polymorph to turn olran into a giant ape to grant him greater strength.
...

The crowd group around olran to help.
From inside the circle a firewall is cast which divides the room.
Olran, with all his effort manages to pull the mask back through the portal, but the the ifrit's hand remains holding hard but gets pulled through the gap.

We stand, looking at a giant ape, holding a mask, held by a fire elemental through a blue portal disk, with 3 gnomes chanting, while the rest of us stand and watch...

The [Olranutan](Olran%20Jacob.md) is born.

Quentin manages to take an attack against the ifrits arm, resulting in the grasp being lost and olran manages to capture the [Moonstone Mask](Moonstone%20Mask.md).


## Next Time

- See what we can learn about the [Moonstone Mask](Moonstone%20Mask.md)
- Check the fancy bedroom for clues
- Deal with the elemental stuck in a ball
