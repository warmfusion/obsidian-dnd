Previous: Lost to time itself
Next: [Game 16 - 20-07-21](Game%2016%20-%2020-07-21.md)


Due to poor backup practices by Mr Toby Jackson, the notes from Games 1-15 are lost to the annals of time and space.

An upgrade to his macbook for work resulted in a machine format ("Whats the worst that can happen...?") and as Obsidian doesn't force update to git the changes were lost.

---

Lessons have been learnt
