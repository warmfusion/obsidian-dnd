
## Players

- [Thomas Llewelyn](Thomas%20Llewelyn.md)- Toby
- [Falcia](Falcia.md)- Amy 
- [[Olran Jacob]]- Matt R.
- [[Ser Quentin Quincy]]- Liam
- [[Muradin ShadyBeard]]- Matt S.


## People Of Significance

[[The Warmonger]]- An unknown horror that used [[Fatihla]] as a weapon

[[Fatihla]]- An ifrit trapped in an ornate dagger by [[Neephael]], owned now by [Falcia](Falcia.md).  The Butcher of [[Kassamir]]worked for the [[The Warmonger]]who then betrayed her.

[[Neephael]]- A celestia multi-planar angel, reincarnated [Thomas Llewelyn](Thomas%20Llewelyn.md), trapped [[Fatihla]]in a blade

### Other Folks

[[Eustace]]- North Gate City Watch
[[Mikkra Bodello]]- ??
[[Corina]]- A child, given a dagger by [[Muradin ShadyBeard]] in the slums of [[NeverWinter]] to defend herself
[[Otto]] - [[Corina]]'s brother

## Places

[[Kassamir]]- The first plane destroyed by [[Fatihla]]