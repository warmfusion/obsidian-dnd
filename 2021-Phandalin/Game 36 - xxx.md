Previous: [Game 35 - 27-09-2022](Game%2035%20-%2027-09-2022.md)
next: [Game 37 - 29-11-2022](Game%2037%20-%2029-11-2022.md)

From memory;

---
![](https://ca.slack-edge.com/T040LKKJH-UCNT9J0HM-a37fdaa26811-48) Guillaume Degremont  [12:54](https://futureplc.slack.com/archives/G01EWQUQTT3/p1669726492814319)  

I am pretty sure we had another session since then, one where the devil was defeated and there were shenanigans with a free efreet

![](https://ca.slack-edge.com/T040LKKJH-UP7L93PN2-9366c6d971f6-48) Matt Sturdy![:spiral_calendar_pad:](https://slack-imgs.com/?c=1&o1=gu&url=https%3A%2F%2Fa.slack-edge.com%2Fproduction-standard-emoji-assets%2F14.0%2Fapple-large%2F1f5d3-fe0f.png)  [13:07](https://futureplc.slack.com/archives/G01EWQUQTT3/p1669727222100389)  

Yeah we definitely had a round two encounter with efreet + devil and nearly all died I think Muradrin got charmed and tried to kill everyone and orlan made a bit of a deal for some extra power…. Did we end back at the house of the FF with some pickle shenanigans? ![:fire:](https://a.slack-edge.com/production-standard-emoji-assets/14.0/apple-medium/1f525@2x.png) ![:five:](https://a.slack-edge.com/production-standard-emoji-assets/14.0/apple-medium/0035-fe0f-20e3@2x.png)

![](https://ca.slack-edge.com/T040LKKJH-UB9B8R2AE-03d0b6731eb2-48) Matt Rink  [16:08](https://futureplc.slack.com/archives/G01EWQUQTT3/p1669738087040499)  

Olran made _another_ bad deal with his new friend in the amulet, which got him one burn/brand/flesh-wound from it in return for a powerful attack against the devil.

---


Thomas picked up his gear
Quentin was given the Cucumber

Went back to house to recover
