Location: [NeverWinter](NeverWinter.md)

Deeds to small 3 bedroom place in the noble quarter near the fallen tower.

Given to us by [salazar the wizard](salazar%20the%20wizard.md) on completion of some quests.
