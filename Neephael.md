Name: Neephael
Race: Celestial
FriendOrFoe: Friend

Affiliations:

## Notes

A Celestial being that trapped [Fatihla](Fatihla.md) in a blade which is now in the posession of [Falcia](Falcia.md).

During an early game [now lost in time](Game%201-15%20-%20Lost%20In%20Time.md) [Thomas Llewelyn](Thomas%20Llewelyn.md) was killed and ressurected by [Neephael](Neephael.md) - We're not really sure why though.



Tags:
[Character](Character.md)[[BigBad]]

 