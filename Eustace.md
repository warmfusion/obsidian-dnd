Name: Eustace
Race:
FriendOrFoe: Friend

Affiliations:
[[Neverwinter City Guard North Watch]]

## Notes

[[Eustace]] owes us a favour as we didn't get any gold as there was a complaint about how we handled the death of the golddigger clan [Game 37 - 29-11-2022](Game%2037%20-%2029-11-2022.md)


Tags:
[Character](Character.md)
