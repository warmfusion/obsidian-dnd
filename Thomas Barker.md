Name: Thomas Barker
Race: Half Elf
FriendOrFoe: Friend

Affiliations:

## Notes

Father of [Thomas Llewelyn](Thomas%20Llewelyn.md) a teacher in [NeverWinter](NeverWinter.md)s [[Star Shine academy]]

See [Who is Thomas](Who%20is%20Thomas.md)



Tags:
[Character](Character.md)
