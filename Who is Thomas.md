[Thomas Llewelyn](Thomas%20Llewelyn.md)


- Charismatic rural bard
- storyteller and collector
- Driven by his ego


Thomas is an well meaning, charismatic but incredibly egotisic bard, who after years of telling the stories, tales and songs of adventures decides that its his time to make the stories and not just tell them anymore.

Thomas is a story teller, able to hold a crowd with his every word, figurativly painting images with his words more colourful and dynamic than the reality they are based on.

Thomas stands out in a crowd when he wants to but can just as easily slip into obscurity in plain sight through his blend of charismatic diversions and character acting.

Thomas keeps a meticulous journal, adding rubbings, pressings and drawings of the travels hes taken. Moving from music event, story telling gala, any cultural hub or fayre has an almost magnetic.

A hidden past he's not keen to share and instead distracts the conversation with tales of others or placing himself at the center of the heroics.


## The Known Story of Thomas



---

⚠️⚠️⚠️ ***Warning*** : Do not read beyond this point if you're playing DnD with me ⚠️⚠️⚠️

---


## The Origin of Thomas

The known story of Thomas is a true depiction of his recent past, and the travels, and ambition that has led him to the current days, however dig deeper into his past and you'll find an origin far more unexpected.


> Thomas is the living portrait of king `Gyeong Geon'Han the Third`, from the far away kingdom of  `Beojkkoch-Ui Nala`


# Archive

These are personal notes used to populate the above section, they are not canonical.

## Original Backstory

Gareth Llewlyn, the Noble Lord, was the 7th son and ruler of the lands of xxxxx. He had 6 sons to his name, and his lady was expecting their seventh child. Praying for a son, when the lady went into labor tragedy struck; after a challenging birth, the lady passed away and left a daughter to the family.

The noble lord withdrew from his court in sorrow, his court story teller, Thomas Barker, was the only relief from the darkness clouding his mind. At first, the lord was distracted by the stories, but over time became facinated by the worlds and creatures the storyteller could conjour with his words.

Gareth, under the mentorship of Thomas became a story-teller himself,  obsessed with the power of stories and together form a powerful partnership as they push the boundaries of story.

After many years of lyrical experiments, exploring the nuances of verbs, metaphor and structure the noble summons his family and the court to a performance

Together, Gareth and Thomas spin their tale, telling of the son Gareth and his lady could never have, the world and history he inhabited and with Gareths devotion and his heartfelt words a being is brought into existence before the assembled courts eyes; Thomas Llewelyn.

Thomas is brought into this plane of existence the same age as his sister, her twin and the seventh son his noble parents were expecting. However, unlike his siblings his parentage is a blend of the Elven noble and human story teller and so he is born a half-elf.

The court rejoices at the arrival of the Nobles seventh son, with the heavy expectations of greatness and power bestoed upon him as the seventh son to the seventh son.

Thomas' 6 brothers and sister never accept him into the family, while Gareth adores Thomas, believing him to be the true heir to his throne and spoils Thomas with every wish he desires. Thomas as such developers an extraordinary ego to match his priviledged position.

Thomas was created to be a member of the family, he has an unwavable love of his brothers and sister, and while they appear to hate him he truly believes he simply needs to proove himself.

He leaves home to make his name his own, and not simply known as the creation of his fathers. He travels further and further trying to leave his legend behind so he can begin to earn his reputation.

To escape his herriage Thomas leaves his nobel upbringing and joins the rustic townsfolk, but struggles to escape his family story. Further and further he travels, taking on the disguise of a story teller himself, collecting legends and stories, waiting for the town where his family story is no longer told, when he can finally begin to craft his own tale.

As the creation of powerful story tellers, Thomas finds himself atuned to the bardic skills, highly charismatic and adaptable he easily slips into the role of a bumbling travling bard. Regardless, Thomas is the 7th son to a 7th son, and so has powers and skills beyond the normal mortals, storys and magic came easily to Thomas; he simply 'knew' these skills and never had to earn anything for himself.


## Public Thomas

- Charismatic rural bard
- storyteller and collector
- Driven by his ego

Thomas is an well meaning, charismatic but incredibly egotisic bard, who after years of telling the stories, tales and songs of adventures decides that its his time to make the stories and not just tell them anymore.

Thomas is a story teller, able to hold a crowd with his every word, figurativly painting images with his words more colourful and dynamic than the reality they are based on.

Thomas keeps a meticulous journal, adding rubbings, pressings and drawings of the travels hes taken. Moving from music event, story telling gala, any cultural hub or fayre has an almost magnetic 


## Hidden Thomas

- Noble origins
- A magical creation
- Driven to make his own mark on the world to earn his place beside his siblings
	- Verges on an obsession; He never feels he'll be renouned enough to return home

Thomas maintains the appearance of a well meaning, interested bard trying to learn about cultures and stories from the world, however hes realy working to find the edge of his family story. Once he reaches the edge of his family legacy, he can begin to create his new story.

## Family

Father (Lord) - Gareth Llewelyn - Adores Thomas to a fault. Practically ignores his other children, believing that Thomas has a greater destiny to fulfil.

Father (Half-Elf) [[Thomas Barker]] - Proud of his own skills to bring Thomas to being, doesn't see Thomas as his true son, but is protective and supportive. Thomas L. loves Thomas B without compare. 

The eldest brother (Elf) - Aled Llewelyn, true heir to the lands has the most to lose from Thomas's existence. He believes Thomas will somehow claim the throne.

The sister (Elf) - Gwen Llewelyn  was effectivly ignroed by her father, believing her to be the cause of her mothers death. She sees Thomas as a cruel betrayal by her father, a tangible demonstration of her failing as a child. She is driven, determined and her brothers are fiercly protective of her. She is a sly, inteligent and dangerous individual, applying the skills of her 6 brothers to fullfil her interests.

All the Siblings (Order of age)

- Aled Llewelyn
	- "Defender"
	- Paladin
- Bryn Llewelyn
	- "Great hill, a hill."
	- Barbarian
- Carwyn Llewelyn
	- "Fair love, pure love, blessed love."
	- Druid
- Dylan Llewelyn
	- "Son of the sea, from a large sea, of the sea."
	- Ranger
- Emyr Llewelyn
	- "King, ruler, honour."
	-  ??
- Floyd Llewelyn
	- "Grey or white haired, gray or white haired, gray, gray haired person, the hollow, grey, gray-haired."
	- Wizard/Mage
- Gwen
	- "Fair-haired"
	- ??

