Name: Thomas Llewelyn
Race: Half-Elf
Class: Bard/Warlock
FriendOrFoe: Friend

Affiliations:
[[Flamous Five]] - [[Sword Coast Miners Guild]] - 

Links:
https://www.dndbeyond.com/characters/38889830


## Notes

- Reincarnated by [Neephael](Neephael.md)
- 


Tags:
[Character](Character.md)
