Name: Fatihla
Race: Ifrit
FriendOrFoe: ItsComplicated

Affiliations:

## Notes

An ifrit trapped in an ornate dagger by [[Neephael]], owned now by [Falcia](Falcia.md).  The Butcher of [[Kassamir]] worked for the [[The Warmonger]] who then betrayed her.


Tags:
[[Character]] - [[BigBad]]

 