Name:  Corina
Race: Unknown
FriendOrFoe: Friendly Ish

Affiliations:
[Otto](Otto) (Brother)

## Notes

A child, [given a dagger](Game%2037%20-%2029-11-2022.md) by [[Muradin ShadyBeard]] in the slums of [[NeverWinter]] to defend herself.

Tags:
[Character](Character.md)
