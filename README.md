# DND Notes


Notes from my games of DnD

## Start here

- [Phandalin and beyond](Game%201-15%20-%20Lost%20In%20Time.md) 
	- A game with 5 players; [Thomas Llewelyn](Thomas%20Llewelyn.md) (Toby), [Falcia](Falcia.md)(Amy), [Olran Jacob](Olran%20Jacob.md)(Matt R.), [Muradin ShadyBeard](Muradin%20ShadyBeard.md)(Matt S), [Ser Quentin Quincy](Ser%20Quentin%20Quincy.md)(Liam)
- [Game 1 - 2021-08-18](Game%201%20-%202021-08-18.md)
